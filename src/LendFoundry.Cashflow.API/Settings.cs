﻿using LendFoundry.Foundation.Services.Settings;

namespace LendFoundry.Cashflow.API
{
    public static class Settings
    {
        private const string Prefix = "cashflow";

        public const string LogServiceName = "cashflow";

        public static string ServiceName { get; } = "cashflow";

        public static ServiceSettings EventHub { get; } = new ServiceSettings($"{Prefix}_EVENTHUB", "eventhub");

        public static ServiceSettings Configuration { get; } = new ServiceSettings($"{Prefix}_CONFIGURATION", "configuration");

        public static ServiceSettings Tenant { get; } = new ServiceSettings($"{Prefix}_TENANT", "tenant");

        public static DatabaseSettings Mongo { get; } = new DatabaseSettings("mongodb://aarzoo:abc123@127.0.0.1:27017/", "mongodb://aarzoo:abc123@127.0.0.1:27017/",
            "cashflow", "cashflow");

    }
}
