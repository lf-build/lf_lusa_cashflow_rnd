﻿using LendFoundry.Cashflow.Contracts;
using LendFoundry.Cashflow.DataModels.Bank;
using LendFoundry.Cashflow.DataModels.Common;
using LendFoundry.Cashflow.DataModels.Summary;
using LendFoundry.Cashflow.Persistence;
using Microsoft.AspNet.Mvc;
using System;
using LendFoundry.Foundation.Services;
using LendFoundry.Cashflow.Abstractions;
using System.Linq.Expressions;
using MongoDB.Driver;
using MongoDB.Driver.Linq;
using System.Collections.Generic;
using System.Linq;
using LendFoundry.Cashflow.DataModels.Customer;

namespace LendFoundry.Cashflow.Services
{
    public class CashflowService : ICashflowService
    {
        #region << Tenant Service >>
        public Tenant.Client.ITenantService TenantService { get; set; }
        #endregion

        #region << Private Members >>
        private MongoConfig _MongoConfig { get; set; }
        private enum TotalCountFlag
        {
            TotalNumberofDeposits = 1,
            TotalNumberofWithdrawals = 2
        }
        private enum TotalSumFlag
        {
            TotalDepositAmount = 1,
            TotalFees = 2,
            TotalBalance = 3,
            TotalWithdrawalAmount = 4
        }
        #endregion

        #region << Constructor >>
        public CashflowService()
        {
            _MongoConfig = new MongoConfig();
            _MongoConfig.ConnectionString = API.Settings.Mongo.ConnectionString;
            _MongoConfig.Database = API.Settings.Mongo.Database;
        }
        #endregion

        #region << Data Feed >>
        /// <summary>
        /// Save the cashflowData into mongo database
        /// </summary>
        /// <param name="cashflowDataModels">CashflowDataModels object</param>
        /// <returns>Status (Error | Success)</returns>
        public ObjectResult Save(CashflowDataModels cashflowDataModels)
        {
            ObjectResult result = null;
            bool flag = IsValid(cashflowDataModels, out result);

            if (flag)
            {
                CashflowDataRepository cashflowRepository = new CashflowDataRepository(TenantService, _MongoConfig.GetConfiguration());
                cashflowRepository.Add(CashflowData.GetData(cashflowDataModels));
            }

            return result;
        }

        /// <summary>
        /// To validate CashflowDataModels object per schema (required fields)
        /// </summary>
        /// <param name="cashflowDataModels">CashflowDataModels object</param>
        /// <param name="result">Output parameter per data validation</param>
        /// <returns>bool (true upon valid unless flase)</returns>
        private bool IsValid(CashflowDataModels cashflowDataModels, out ObjectResult result)
        {
            bool flag = true;
            result = new ObjectResult("{\"Status\": \"204\", \"Messsage\": \"Ok\"}");

            if (cashflowDataModels == null)
            {
                flag = false;
                result = new ObjectResult(new Error(402, "Invalid data."));
            }
            else if (cashflowDataModels.BankAccount == null ||
                    cashflowDataModels.BankAccount.FromYear < DateTime.Now.Year - 2 ||
                    cashflowDataModels.BankAccount.ToYear < DateTime.Now.Year - 2 ||
                    cashflowDataModels.BankAccount.FromYear > cashflowDataModels.BankAccount.ToYear)
            {
                flag = false;
                result = new ObjectResult(new Error(402, "Invalid bank account data."));
            }
            else if (cashflowDataModels.BankAccount.Bank == null ||
                    String.IsNullOrWhiteSpace(cashflowDataModels.BankAccount.Bank.Name) ||
                    !IsAddressValid(cashflowDataModels.BankAccount.Bank.Address))
            {
                flag = false;
                result = new ObjectResult(new Error(402, "Bank data is missing."));
            }
            else if (cashflowDataModels.BankAccount.Accounts == null ||
                cashflowDataModels.BankAccount.Accounts.Count < 1 ||
                String.IsNullOrWhiteSpace(cashflowDataModels.BankAccount.CustomerReferenceNumber))
            {
                flag = false;
                result = new ObjectResult(new Error(402, "Accounts information is missing."));
            }
            else
            {
                foreach (AccountModels account in cashflowDataModels.BankAccount.Accounts)
                {
                    if (String.IsNullOrWhiteSpace(account.AccountNumber) ||
                        account.StatementEntries == null ||
                        account.StatementEntries.Count < 1)
                    {
                        flag = false;
                        result = new ObjectResult(new Error(402, "Bank account information is missing."));
                        break;
                    }
                    else if (account.Customer == null ||
                        account.Customer.CustomerName == null ||
                        account.Customer.CustomerName.Count < 1)
                    {
                        flag = false;
                        result = new ObjectResult(new Error(402, "Customer information is missing."));
                        break;
                    }
                    else if (account.Customer.Address == null ||
                        !IsAddressValid(account.Customer.Address))
                    {
                        flag = false;
                        result = new ObjectResult(new Error(402, "Customer address is missing."));
                        break;
                    }
                    else
                    {
                        foreach (CustomerNameModels customerName in account.Customer.CustomerName)
                        {
                            if (String.IsNullOrWhiteSpace(customerName.NameOnTheAccount))
                            {
                                flag = false;
                                result = new ObjectResult(new Error(402, "Customer name is missing."));
                                break;
                            }
                        }

                        foreach (StatementEntryModels sem in account.StatementEntries)
                        {
                            if (sem.Amount == null || sem.Date.Year < DateTime.Now.Year - 2)
                            {
                                flag = false;
                                result = new ObjectResult(new Error(402, "Bank statement information is missing."));
                                return flag;
                            }
                        }
                    }
                }
            }

            return flag;
        }
        private bool IsAddressValid(AddressModels addressModels)
        {
            bool flag = true;
            if (addressModels == null ||
                    String.IsNullOrWhiteSpace(addressModels.Address1) ||
                    String.IsNullOrWhiteSpace(addressModels.City) ||
                    String.IsNullOrWhiteSpace(addressModels.State) ||
                    String.IsNullOrWhiteSpace(addressModels.ZipCode) ||
                    String.IsNullOrWhiteSpace(addressModels.Country))
            {
                flag = false;
            }

            return flag;
        }
        #endregion

        #region << Get Methods >>
        //1.	GET /cashflow/{customerReferenceNumber}
        public MonthlySummaryModels AccountSummary(string customerReferenceNumber)
        {
            Expression<Func<ICashflowData, bool>> query = p =>
                p.BankAccount.CustomerReferenceNumber == customerReferenceNumber;

            return GetData(query);
        }

        //2.	GET /cashflow/{customerReferenceNumber}/{accountNumber}
        public MonthlySummaryModels AccountSummary(string customerReferenceNumber, string accountNumber)
        {
            Expression<Func<ICashflowData, bool>> query = p =>
                p.BankAccount.CustomerReferenceNumber == customerReferenceNumber;

            return GetData(query, accountNumber);
        }

        //3.	GET /cashflow/{customerReferenceNumber}/{startDate}/{endDate}
        public MonthlySummaryModels AccountSummary(string customerReferenceNumber, DateTime startDate, DateTime endDate)
        {
            Expression<Func<ICashflowData, bool>> query = p =>
                p.BankAccount.CustomerReferenceNumber == customerReferenceNumber;

            return GetData(query, startDate, endDate);
        }
        public MonthlySummaryModels AccountSummary(string customerReferenceNumber, DateTime date)
        {
            Expression<Func<ICashflowData, bool>> query = p =>
                p.BankAccount.CustomerReferenceNumber == customerReferenceNumber;

            return GetData(query, date);
        }

        //4.	GET /cashflow/{customerReferenceNumber}/totalDepositAmount
        public Single TotalDepositAmount(string customerReferenceNumber)
        {
            return AccountSummary(customerReferenceNumber).TotalDepositAmount;
        }

        //5.	GET /cashflow/{customerReferenceNumber}/totalDepositAmount/{startDate}/{endDate}
        public Single TotalDepositAmount(string customerReferenceNumber, DateTime startDate, DateTime endDate)
        {
            return AccountSummary(customerReferenceNumber, startDate, endDate).TotalDepositAmount;
        }

        //6.	GET /cashflow/{customerReferenceNumber}/totalDepositCount
        public Int32 TotalDepositCount(string customerReferenceNumber)
        {
            return AccountSummary(customerReferenceNumber).TotalNumberofDeposits;
        }

        //7.	GET /cashflow/{customerReferenceNumber}/totalDepositCount/{startDate}/{endDate}
        public Int32 TotalDepositCount(string customerReferenceNumber, DateTime startDate, DateTime endDate)
        {
            return AccountSummary(customerReferenceNumber, startDate, endDate).TotalNumberofDeposits;
        }

        //8.	GET /cashflow/{customerReferenceNumber}/totalWithdrawalAmount
        public Single TotalWithdrawalAmount(string customerReferenceNumber)
        {
            return AccountSummary(customerReferenceNumber).TotalWithdrawalAmount;
        }

        //9.	GET /cashflow/{customerReferenceNumber}/totalWithdrawalAmount/{startDate}/{endDate}
        public Single TotalWithdrawalAmount(string customerReferenceNumber, DateTime startDate, DateTime endDate)
        {
            return AccountSummary(customerReferenceNumber, startDate, endDate).TotalWithdrawalAmount;
        }

        //10.	GET /cashflow/{customerReferenceNumber}/totalWithdrawalCount
        public Int32 TotalWithdrawalCount(string customerReferenceNumber)
        {
            return AccountSummary(customerReferenceNumber).TotalNumberofWithdrawals;
        }

        //11.	GET /cashflow/{customerReferenceNumber}/totalWithdrawalCount/{startDate}/{endDate}
        public Int32 TotalWithdrawalCount(string customerReferenceNumber, DateTime startDate, DateTime endDate)
        {
            return AccountSummary(customerReferenceNumber, startDate, endDate).TotalNumberofWithdrawals;
        }

        //12.	GET /cashflow/{customerReferenceNumber}/averageDailyBalance
        public Single AverageDailyBalance(string customerReferenceNumber)
        {
            return AccountSummary(customerReferenceNumber).AverageDailyBalanace;
        }

        //13.	GET /cashflow/{customerReferenceNumber}/averageDailyBalance/{startDate}/{endDate}
        public Single AverageDailyBalance(string customerReferenceNumber, DateTime startDate, DateTime endDate)
        {
            return AccountSummary(customerReferenceNumber, startDate, endDate).AverageDailyBalanace;
        }

        //14.	GET /cashflow/{customerReferenceNumber}/totalNegativeDays
        public Int32 TotalNegativeDays(string customerReferenceNumber)
        {
            //TODO: Need to implement later
            return 0;
        }

        //15.	GET /cashflow/{customerReferenceNumber}/totalNegativeDays/{startDate}/{endDate}
        public Int32 TotalNegativeDays(string customerReferenceNumber, DateTime startDate, DateTime endDate)
        {
            //TODO: Need to implement later
            return 0;
        }

        //16.	GET /cashflow/{customerReferenceNumber}/beginningBalance
        public Single BeginningBalance(string customerReferenceNumber)
        {
            return AccountSummary(customerReferenceNumber).BeginningBalance;
        }

        //17.	GET /cashflow/{customerReferenceNumber}/beginningBalance/{date}
        public Single BeginningBalance(string customerReferenceNumber, DateTime date)
        {
            return AccountSummary(customerReferenceNumber, date).BeginningBalance;
        }

        //18.	GET /cashflow/{customerReferenceNumber}/endingBalance
        public Single EndingBalance(string customerReferenceNumber)
        {
            return AccountSummary(customerReferenceNumber).EndingBalance;
        }

        //19.	GET /cashflow/{customerReferenceNumber}/endingBalance/{date}
        public Single EndingBalance(string customerReferenceNumber, DateTime date)
        {
            return AccountSummary(customerReferenceNumber, date).EndingBalance;
        }

        //20.	GET /cashflow/{customerReferenceNumber}/totalFees
        public Single TotalFees(string customerReferenceNumber)
        {
            return AccountSummary(customerReferenceNumber).TotalFees;
        }

        //21.	GET /cashflow/{customerReferenceNumber}/totalFees/{date}
        public Single TotalFees(string customerReferenceNumber, DateTime date)
        {
            return AccountSummary(customerReferenceNumber, date).TotalFees;
        }

        //22.	GET /cashflow/{customerReferenceNumber}/totalNsf
        public Single TotalNsf(string customerReferenceNumber)
        {
            //TODO: Need to implement later
            return 0;
        }

        //23.	GET /cashflow/{customerReferenceNumber}/totalNsf/{startDate}/{endDate}
        public Single TotalNsf(string customerReferenceNumber, DateTime startDate, DateTime endDate)
        {
            //TODO: Need to implement later
            return 0;
        }
        #endregion

        #region << Private Methods >>
        private Single TotalSumAmount(List<StatementEntryModels> statements, TotalSumFlag totalSumFlag)
        {
            IEnumerable<Single> TotalAmount = null;

            if (totalSumFlag == TotalSumFlag.TotalDepositAmount)
            {
                TotalAmount = from bs in statements
                              where bs.EntryBaseType == DataModels.Enum.TransactionType.Credit
                              group bs by bs.EntryBaseType into g
                              select g.Sum(p => p.Amount.Amount);
            }
            else if (totalSumFlag == TotalSumFlag.TotalFees)
            {
                TotalAmount = from bs in statements
                              where bs.EntryBaseType == DataModels.Enum.TransactionType.Debit && bs.IsServiceFee == true
                              group bs by bs.EntryBaseType into g
                              select g.Sum(p => -(p.Amount.Amount));
            }
            else if (totalSumFlag == TotalSumFlag.TotalBalance)
            {
                TotalAmount = from bs in statements
                              group bs by bs.EntryBaseType into g
                              select g.Sum(p => p.Amount.Amount);
            }
            else if (totalSumFlag == TotalSumFlag.TotalWithdrawalAmount)
            {
                TotalAmount = from bs in statements
                              where bs.EntryBaseType == DataModels.Enum.TransactionType.Debit
                              group bs by bs.EntryBaseType into g
                              select g.Sum(p => p.Amount.Amount);
            }

            if (TotalAmount != null && TotalAmount.ToList().Count > 0)
            {
                return TotalAmount.ToList()[0];
            }
            return 0;
        }

        private int TotalNumberInfo(List<StatementEntryModels> statements, TotalCountFlag totalCountFlag)
        {
            IEnumerable<int> TotalAmount = null;

            if (totalCountFlag == TotalCountFlag.TotalNumberofDeposits)
            {
                TotalAmount = from bs in statements
                              where bs.EntryBaseType == DataModels.Enum.TransactionType.Credit
                              group bs by bs.EntryBaseType into g
                              select g.Count();
            }
            else if (totalCountFlag == TotalCountFlag.TotalNumberofWithdrawals)
            {
                TotalAmount = from bs in statements
                              where bs.EntryBaseType == DataModels.Enum.TransactionType.Debit
                              group bs by bs.EntryBaseType into g
                              select g.Count();
            }

            if (TotalAmount != null && TotalAmount.ToList().Count > 0)
            {
                return TotalAmount.ToList()[0];
            }

            return 0;
        }

        private List<ICashflowData> GetSummaryData(Expression<Func<ICashflowData, bool>> query)
        {
            CashflowDataRepository cashflowRepository = new CashflowDataRepository(TenantService, _MongoConfig.GetConfiguration());
            List<ICashflowData> data = cashflowRepository.GetCollection().AsQueryable<ICashflowData>().Where(query).ToList();

            return data;
        }

        private MonthlySummaryModels GetData(Expression<Func<ICashflowData, bool>> query)
        {
            return GetData(query, null);
        }

        private MonthlySummaryModels GetData(Expression<Func<ICashflowData, bool>> query, string accountNumber)
        {
            MonthlySummaryModels monthlySummaryModels = new MonthlySummaryModels();
            List<ICashflowData> data = GetSummaryData(query);

            if (data != null)
            {
                monthlySummaryModels.Month = data[0].BankAccount.FromMonth;
                monthlySummaryModels.Year = data[0].BankAccount.FromYear;
                monthlySummaryModels.TotalNegativeDays = 0;
                monthlySummaryModels.TotalNonSufficientFundFees = 0;
                bool flag = false;

                foreach (AccountModels am in data[0].BankAccount.Accounts)
                {
                    flag = false;
                    if (String.IsNullOrWhiteSpace(accountNumber) || accountNumber == am.AccountNumber)
                    {
                        flag = true;
                    }

                    if (flag)
                    {
                        monthlySummaryModels.TotalDepositAmount += TotalSumAmount(am.StatementEntries, TotalSumFlag.TotalDepositAmount);
                        monthlySummaryModels.TotalWithdrawalAmount += TotalSumAmount(am.StatementEntries, TotalSumFlag.TotalWithdrawalAmount);
                        monthlySummaryModels.TotalNumberofDeposits += TotalNumberInfo(am.StatementEntries, TotalCountFlag.TotalNumberofDeposits);
                        monthlySummaryModels.TotalNumberofWithdrawals += TotalNumberInfo(am.StatementEntries, TotalCountFlag.TotalNumberofWithdrawals);
                        monthlySummaryModels.AverageDailyBalanace += (TotalSumAmount(am.StatementEntries, TotalSumFlag.TotalBalance) / 30);

                        monthlySummaryModels.BeginningBalance += am.BeginningBalance;
                        monthlySummaryModels.EndingBalance += am.EndingBalance;
                        monthlySummaryModels.TotalFees += TotalSumAmount(am.StatementEntries, TotalSumFlag.TotalFees);
                    }
                }
            }

            return monthlySummaryModels;
        }

        private MonthlySummaryModels GetData(Expression<Func<ICashflowData, bool>> query, DateTime date)
        {
            MonthlySummaryModels monthlySummaryModels = new MonthlySummaryModels();
            List<ICashflowData> data = GetSummaryData(query);

            if (data != null)
            {
                monthlySummaryModels.Month = data[0].BankAccount.FromMonth;
                monthlySummaryModels.Year = data[0].BankAccount.FromYear;
                monthlySummaryModels.TotalNegativeDays = 0;
                monthlySummaryModels.TotalNonSufficientFundFees = 0;

                foreach (AccountModels am in data[0].BankAccount.Accounts)
                {
                    IEnumerable<StatementEntryModels> Statements = from stmnt in am.StatementEntries
                                                                   where stmnt.Date == date
                                                                   select stmnt;

                    List<StatementEntryModels> statements = Statements.ToList();

                    if (statements.Count > 0)
                    {
                        monthlySummaryModels.TotalDepositAmount += TotalSumAmount(statements, TotalSumFlag.TotalDepositAmount);
                        monthlySummaryModels.TotalNumberofDeposits += TotalNumberInfo(statements, TotalCountFlag.TotalNumberofDeposits);
                        monthlySummaryModels.TotalNumberofWithdrawals += TotalNumberInfo(statements, TotalCountFlag.TotalNumberofWithdrawals);
                        monthlySummaryModels.AverageDailyBalanace += TotalSumAmount(statements, TotalSumFlag.TotalBalance);

                        monthlySummaryModels.BeginningBalance += am.BeginningBalance;
                        monthlySummaryModels.EndingBalance += am.EndingBalance;
                        monthlySummaryModels.TotalFees += TotalSumAmount(statements, TotalSumFlag.TotalFees);
                    }
                }
            }

            return monthlySummaryModels;
        }

        private MonthlySummaryModels GetData(Expression<Func<ICashflowData, bool>> query, DateTime startDate, DateTime endDate)
        {
            MonthlySummaryModels monthlySummaryModels = new MonthlySummaryModels();
            List<ICashflowData> data = GetSummaryData(query);

            if (data != null)
            {
                int days = (int)(endDate - startDate).TotalDays;
                days++; //includeing end date
                monthlySummaryModels.Month = data[0].BankAccount.FromMonth;
                monthlySummaryModels.Year = data[0].BankAccount.FromYear;
                monthlySummaryModels.TotalNegativeDays = 0;
                monthlySummaryModels.TotalNonSufficientFundFees = 0;

                foreach (AccountModels am in data[0].BankAccount.Accounts)
                {
                    IEnumerable<StatementEntryModels> Statements = from stmnt in am.StatementEntries
                                                                   where stmnt.Date >= startDate &&
                                                                   stmnt.Date <= endDate
                                                                   select stmnt;

                    List<StatementEntryModels> statements = Statements.ToList();

                    if (statements.Count > 0)
                    {
                        monthlySummaryModels.TotalDepositAmount += TotalSumAmount(statements, TotalSumFlag.TotalDepositAmount);
                        monthlySummaryModels.TotalNumberofDeposits += TotalNumberInfo(statements, TotalCountFlag.TotalNumberofDeposits);
                        monthlySummaryModels.TotalNumberofWithdrawals += TotalNumberInfo(statements, TotalCountFlag.TotalNumberofWithdrawals);
                        monthlySummaryModels.AverageDailyBalanace += (TotalSumAmount(statements, TotalSumFlag.TotalBalance) / days);

                        monthlySummaryModels.BeginningBalance += am.BeginningBalance;
                        monthlySummaryModels.EndingBalance += am.EndingBalance;
                        monthlySummaryModels.TotalFees += TotalSumAmount(statements, TotalSumFlag.TotalFees);
                    }
                }
            }

            return monthlySummaryModels;
        }
        #endregion
    }
}
