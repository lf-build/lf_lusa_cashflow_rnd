﻿using Microsoft.AspNet.Mvc;
using System;
using LendFoundry.Cashflow.DataModels.Summary;
using LendFoundry.Cashflow.DataModels.Common;

namespace LendFoundry.Cashflow.Contracts
{
    public interface ICashflowService
    {
        #region << Tenant Service >>
        Tenant.Client.ITenantService TenantService { get; set; }
        #endregion

        #region << Data Feed >>
        ObjectResult Save(CashflowDataModels CashflowDataModels);
        #endregion

        #region << Get Methods >>
        //1.	GET /cashflow/{customerReferenceNumber}
        MonthlySummaryModels AccountSummary(string customerReferenceNumber);

        //2.	GET /cashflow/{customerReferenceNumber}/{accountNumber}
        MonthlySummaryModels AccountSummary(string customerReferenceNumber, string accountNumber);

        //3.	GET /cashflow/{customerReferenceNumber}/{startDate}/{endDate}
        MonthlySummaryModels AccountSummary(string customerReferenceNumber, DateTime startDate, DateTime endDate);

        //4.	GET /cashflow/{customerReferenceNumber}/totalDepositAmount
        Single TotalDepositAmount(string customerReferenceNumber);

        //5.	GET /cashflow/{customerReferenceNumber}/totalDepositAmount/{startDate}/{endDate}
        Single TotalDepositAmount(string customerReferenceNumber, DateTime startDate, DateTime endDate);

        //6.	GET /cashflow/{customerReferenceNumber}/totalDepositCount
        Int32 TotalDepositCount(string customerReferenceNumber);

        //7.	GET /cashflow/{customerReferenceNumber}/totalDepositCount/{startDate}/{endDate}
        Int32 TotalDepositCount(string customerReferenceNumber, DateTime startDate, DateTime endDate);

        //8.	GET /cashflow/{customerReferenceNumber}/totalWithdrawalAmount
        Single TotalWithdrawalAmount(string customerReferenceNumber);

        //9.	GET /cashflow/{customerReferenceNumber}/totalWithdrawalAmount/{startDate}/{endDate}
        Single TotalWithdrawalAmount(string customerReferenceNumber, DateTime startDate, DateTime endDate);

        //10.	GET /cashflow/{customerReferenceNumber}/totalWithdrawalCount
        Int32 TotalWithdrawalCount(string customerReferenceNumber);

        //11.	GET /cashflow/{customerReferenceNumber}/totalWithdrawalCount/{startDate}/{endDate}
        Int32 TotalWithdrawalCount(string customerReferenceNumber, DateTime startDate, DateTime endDate);

        //12.	GET /cashflow/{customerReferenceNumber}/averageDailyBalance
        Single AverageDailyBalance(string customerReferenceNumber);

        //13.	GET /cashflow/{customerReferenceNumber}/averageDailyBalance/{startDate}/{endDate}
        Single AverageDailyBalance(string customerReferenceNumber, DateTime startDate, DateTime endDate);

        //14.	GET /cashflow/{customerReferenceNumber}/totalNegativeDays
        Int32 TotalNegativeDays(string customerReferenceNumber);

        //15.	GET /cashflow/{customerReferenceNumber}/totalNegativeDays/{startDate}/{endDate}
        Int32 TotalNegativeDays(string customerReferenceNumber, DateTime startDate, DateTime endDate);

        //16.	GET /cashflow/{customerReferenceNumber}/beginningBalance
        Single BeginningBalance(string customerReferenceNumber);

        //17.	GET /cashflow/{customerReferenceNumber}/beginningBalance/{date}
        Single BeginningBalance(string customerReferenceNumber, DateTime date);

        //18.	GET /cashflow/{customerReferenceNumber}/endingBalance
        Single EndingBalance(string customerReferenceNumber);

        //19.	GET /cashflow/{customerReferenceNumber}/endingBalance/{date}
        Single EndingBalance(string customerReferenceNumber, DateTime date);

        //20.	GET /cashflow/{customerReferenceNumber}/totalFees
        Single TotalFees(string customerReferenceNumber);

        //21.	GET /cashflow/{customerReferenceNumber}/totalFees/{date}
        Single TotalFees(string customerReferenceNumber, DateTime date);

        //22.	GET /cashflow/{customerReferenceNumber}/totalNsf
        Single TotalNsf(string customerReferenceNumber);

        //23.	GET /cashflow/{customerReferenceNumber}/totalNsf/{startDate}/{endDate}
        Single TotalNsf(string customerReferenceNumber, DateTime startDate, DateTime endDate);
        #endregion
    }
}