﻿using System;
using Microsoft.AspNet.Mvc;
using LendFoundry.Cashflow.Contracts;
using LendFoundry.Cashflow.DataModels.Common;
using LendFoundry.Foundation.Services;

// For more information on enabling Web API for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace LendFoundry.Cashflow.Controllers
{
    [Route("/")]
    public class CashflowController : Controller
    {
        #region << Private Properties >>
        private ICashflowService CashflowService { get; }
        private Tenant.Client.ITenantService TenantService { get; }
        #endregion

        #region << Controller Constructor >>
        public CashflowController(ICashflowService cashflowService, Tenant.Client.ITenantService tenantService)
        {
            CashflowService = cashflowService;
            TenantService = tenantService;
            CashflowService.TenantService = tenantService;
        }
        #endregion

        [HttpGet("cashflow")]
        public string Get()
        {
            return "Test Cashflow Service";
        }

        #region << Data Feed >>
        [HttpPost("cashflow")]
        public IActionResult Post([FromBody] CashflowDataModels CashflowDataModels)
        {
            ObjectResult result = null;
            try
            {
                result = CashflowService.Save(CashflowDataModels);
            }
            catch (Exception ex)
            {
                result = new ObjectResult(new Error(500, ex.Message));
            }

            return result;
        }
        #endregion

        #region << Get Methods >>
        [HttpGet("cashflow/{customerReferenceNumber}")]
        public IActionResult AccountSummary(string customerReferenceNumber)
        {
            return new ObjectResult(CashflowService.AccountSummary(customerReferenceNumber));
        }

        [HttpGet("cashflow/{customerReferenceNumber}/{accountNumber}")]
        public IActionResult AccountSummary(string customerReferenceNumber, string accountNumber)
        {
            return new ObjectResult(CashflowService.AccountSummary(customerReferenceNumber, accountNumber));
        }

        [HttpGet("cashflow/{customerReferenceNumber}/{startDate}/{endDate}")]
        public IActionResult AccountSummary(string customerReferenceNumber, DateTime startDate, DateTime endDate)
        {
            return new ObjectResult(CashflowService.AccountSummary(customerReferenceNumber, startDate, endDate));
        }

        [HttpGet("cashflow/{customerReferenceNumber}/totalDepositAmount")]
        public IActionResult TotalDepositAmount(string customerReferenceNumber)
        {
            return new ObjectResult(CashflowService.TotalDepositAmount(customerReferenceNumber));
        }

        [HttpGet("cashflow/{customerReferenceNumber}/totalDepositAmount/{startDate}/{endDate}")]
        public IActionResult TotalDepositAmount(string customerReferenceNumber, DateTime startDate, DateTime endDate)
        {
            return new ObjectResult(CashflowService.TotalDepositAmount(customerReferenceNumber, startDate, endDate));
        }

        [HttpGet("cashflow/{customerReferenceNumber}/totalDepositCount")]
        public IActionResult TotalDepositCount(string customerReferenceNumber)
        {
            return new ObjectResult(CashflowService.TotalDepositCount(customerReferenceNumber));
        }

        [HttpGet("cashflow/{customerReferenceNumber}/totalDepositCount/{startDate}/{endDate}")]
        public IActionResult TotalDepositCount(string customerReferenceNumber, DateTime startDate, DateTime endDate)
        {
            return new ObjectResult(CashflowService.TotalDepositCount(customerReferenceNumber, startDate, endDate));
        }

        [HttpGet("cashflow/{customerReferenceNumber}/totalWithdrawalAmount")]
        public IActionResult TotalWithdrawalAmount(string customerReferenceNumber)
        {
            return new ObjectResult(CashflowService.TotalWithdrawalAmount(customerReferenceNumber));
        }

        [HttpGet("cashflow/{customerReferenceNumber}/totalWithdrawalAmount/{startDate}/{endDate}")]
        public IActionResult TotalWithdrawalAmount(string customerReferenceNumber, DateTime startDate, DateTime endDate)
        {
            return new ObjectResult(CashflowService.TotalWithdrawalAmount(customerReferenceNumber, startDate, endDate));
        }

        [HttpGet("cashflow/{customerReferenceNumber}/totalWithdrawalCount")]
        public IActionResult TotalWithdrawalCount(string customerReferenceNumber)
        {
            return new ObjectResult(CashflowService.TotalWithdrawalCount(customerReferenceNumber));
        }

        [HttpGet("cashflow/{customerReferenceNumber}/totalWithdrawalCount/{startDate}/{endDate}")]
        public IActionResult TotalWithdrawalCount(string customerReferenceNumber, DateTime startDate, DateTime endDate)
        {
            return new ObjectResult(CashflowService.TotalWithdrawalCount(customerReferenceNumber, startDate, endDate));
        }

        [HttpGet("cashflow/{customerReferenceNumber}/averageDailyBalance")]
        public IActionResult AverageDailyBalance(string customerReferenceNumber)
        {
            return new ObjectResult(CashflowService.AverageDailyBalance(customerReferenceNumber));
        }

        [HttpGet("cashflow/{customerReferenceNumber}/averageDailyBalance/{startDate}/{endDate}")]
        public IActionResult AverageDailyBalance(string customerReferenceNumber, DateTime startDate, DateTime endDate)
        {
            return new ObjectResult(CashflowService.AverageDailyBalance(customerReferenceNumber, startDate, endDate));
        }

        [HttpGet("cashflow/{customerReferenceNumber}/totalNegativeDays")]
        public IActionResult TotalNegativeDays(string customerReferenceNumber)
        {
            return new ObjectResult(CashflowService.TotalNegativeDays(customerReferenceNumber));
        }

        [HttpGet("cashflow/{customerReferenceNumber}/totalNegativeDays/{startDate}/{endDate}")]
        public IActionResult TotalNegativeDays(string customerReferenceNumber, DateTime startDate, DateTime endDate)
        {
            return new ObjectResult(CashflowService.TotalNegativeDays(customerReferenceNumber, startDate, endDate));
        }

        [HttpGet("cashflow/{customerReferenceNumber}/beginningBalance")]
        public IActionResult BeginningBalance(string customerReferenceNumber)
        {
            return new ObjectResult(CashflowService.BeginningBalance(customerReferenceNumber));
        }

        [HttpGet("cashflow/{customerReferenceNumber}/beginningBalance/{date}")]
        public IActionResult BeginningBalance(string customerReferenceNumber, DateTime date)
        {
            return new ObjectResult(CashflowService.BeginningBalance(customerReferenceNumber, date));
        }

        [HttpGet("cashflow/{customerReferenceNumber}/endingBalance")]
        public IActionResult EndingBalance(string customerReferenceNumber)
        {
            return new ObjectResult(CashflowService.EndingBalance(customerReferenceNumber));
        }

        [HttpGet("cashflow/{customerReferenceNumber}/endingBalance/{date}")]
        public IActionResult EndingBalance(string customerReferenceNumber, DateTime date)
        {
            return new ObjectResult(CashflowService.EndingBalance(customerReferenceNumber, date));
        }

        [HttpGet("cashflow/{customerReferenceNumber}/totalFees")]
        public IActionResult TotalFees(string customerReferenceNumber)
        {
            return new ObjectResult(CashflowService.TotalFees(customerReferenceNumber));
        }

        [HttpGet("cashflow/{customerReferenceNumber}/totalFees/{date}")]
        public IActionResult TotalFees(string customerReferenceNumber, DateTime date)
        {
            return new ObjectResult(CashflowService.TotalFees(customerReferenceNumber, date));
        }

        [HttpGet("cashflow/{customerReferenceNumber}/totalNsf")]
        public IActionResult TotalNsf(string customerReferenceNumber)
        {
            return new ObjectResult(CashflowService.TotalNsf(customerReferenceNumber));
        }
        [HttpGet("cashflow/{customerReferenceNumber}/totalNsf/{startDate}/{endDate}")]

        public IActionResult TotalNsf(string customerReferenceNumber, DateTime startDate, DateTime endDate)
        {
            return new ObjectResult(CashflowService.TotalNsf(customerReferenceNumber, startDate, endDate));
        }
        #endregion
    }
}
