﻿using Microsoft.AspNet.Builder;
using Microsoft.AspNet.Hosting;
using LendFoundry.Cashflow.Persistence;
using LendFoundry.Cashflow.Abstractions;
using LendFoundry.Cashflow.API;
using LendFoundry.Tenant.Client;
using Microsoft.Framework.DependencyInjection;
using LendFoundry.Foundation.Services;
using LendFoundry.Foundation.Date;
using LendFoundry.Security.Client;
using LendFoundry.Foundation.Logging;
using LendFoundry.Security.Tokens;
using System.Configuration;
using Microsoft.Dnx.Runtime;
using Microsoft.Framework.Configuration;

namespace LendFoundry.Cashflow
{
    public class Startup
    {
        public Startup(IHostingEnvironment env)
        {
            
        }
        
        // This method gets called by the runtime. Use this method to add services to the container
        public void ConfigureServices(IServiceCollection services)
        {
            // Add framework services.                        
            services.AddTransient<Contracts.ICashflowService, Services.CashflowService>();
            services.AddTransient<ICashflowData, CashflowData>();
            services.AddTransient<ICashflowDataRepository, CashflowDataRepository>();

            services.AddTenantTime();
            services.AddTokenHandler();
            services.AddHttpServiceLogging(Settings.LogServiceName);
            services.AddTenantService(Settings.Tenant.Host, Settings.Tenant.Port);

            services.AddMvc().AddLendFoundryJsonOptions();
            services.AddCors();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            app.UseCors(env);
            app.UseErrorHandling();
            app.UseSecurityControl();
            app.UseRequestLogging();
            app.UseMvc();
        }

        public static void Main() { }
    }
}
