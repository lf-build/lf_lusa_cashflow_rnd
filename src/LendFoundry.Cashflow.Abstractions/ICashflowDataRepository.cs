﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using LendFoundry.Foundation.Persistence;

namespace LendFoundry.Cashflow.Abstractions
{
    public interface ICashflowDataRepository : IRepository<ICashflowData>
    {
        void Add(IEnumerable<ICashflowData> cashflowData);

        IEnumerable<T> All<T>(Expression<Func<T, bool>> query) where T : ICashflowData;

        void Update(IEnumerable<ICashflowData> cashflowData);

        List<T> GetCashflowData<T>(Expression<Func<T, bool>> query) where T : ICashflowData;
    }
}