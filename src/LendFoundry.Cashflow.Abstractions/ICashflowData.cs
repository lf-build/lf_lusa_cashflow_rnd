﻿using LendFoundry.Cashflow.DataModels.Bank;
using LendFoundry.Foundation.Persistence;
using System;

namespace LendFoundry.Cashflow.Abstractions
{    
    public interface ICashflowData: IAggregate
    {
        //string TenantID { get; set; }
        BankAccountModels BankAccount { get; set; }
        string Description { get; set; }
        string CreatedBy { get; set; }
        DateTime CreatedOn { get; set; }
    }
}
