﻿using LendFoundry.Cashflow.DataModels.Bank;
using LendFoundry.Cashflow.DataModels.Common;
using LendFoundry.Foundation.Persistence;
using MongoDB.Bson.Serialization.Attributes;
using System;

namespace LendFoundry.Cashflow.Abstractions
{
    [BsonIgnoreExtraElements]
    public class CashflowData: Aggregate, ICashflowData
    {
        public BankAccountModels BankAccount { get; set; }
        public string Description { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }

        public static ICashflowData GetData(CashflowDataModels cashflowDataModels)
        {
            CashflowData cashflowData = new CashflowData();
            cashflowData.BankAccount = cashflowDataModels.BankAccount;
            cashflowData.Description = cashflowDataModels.Description;
            cashflowData.CreatedBy = cashflowDataModels.CreatedBy;
            return cashflowData;
        }
    }
}
