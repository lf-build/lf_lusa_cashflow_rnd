﻿using LendFoundry.Cashflow.DataModels.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Cashflow.DataModels.Customer
{
    public class CustomerNameModels
    {
        public string NameOnTheAccount { get; set; } //Required field
        public string FirstName { get; set; }
        public string MI { get; set; }
        public string LastName { get; set; }
        public string Generation { get; set; } /*Sr, Jr*/
        public AccountHolderPosition Position { get; set; } //Required field
    }
}
