﻿using LendFoundry.Cashflow.DataModels.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LendFoundry.Cashflow.DataModels.Customer
{
    /// <summary>
    /// Customer details
    /// </summary>
    public class CustomerModels
    {
        public string CustomerNumber { get; set; }
        public List<CustomerNameModels> CustomerName { get; set; }
        public AddressModels Address { get; set; }
    }
}
