﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Cashflow.DataModels.Enum
{
    public enum CategoryType
    {
        Uncategorize = 1,
        Expense = 2,
        Transfer = 3,
        Income = 4,
        DeferredCompensation = 5,
        Others = 999
    }
}
