﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Cashflow.DataModels.Enum
{
    /// <summary>
    /// Currency code
    /// </summary>
    public enum CurrencyCode
    {
        USD = 1,
        INR = 91,
        Others = 999
    }
}