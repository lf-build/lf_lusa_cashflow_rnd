﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Cashflow.DataModels.Enum
{
    /// <summary>
    /// All credit cards
    /// </summary>
    public enum CreditCardType
    {
        Visa = 1,
        MasterCard = 2,
        AmericanExpress = 3,
        Discover = 4,
        DinersClub = 5,
        Others = 99
    }
}
