﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Cashflow.DataModels.Enum
{
    public enum StatementEntryStatus
    {
        Posted = 1,
        Pending = 2
    }
}
