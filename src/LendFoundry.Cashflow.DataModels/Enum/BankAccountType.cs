﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LendFoundry.Cashflow.DataModels.Enum
{
    /// <summary>
    /// Type of bank account
    /// </summary>
    public enum BankAccountType
    {
        Checking = 1,
        Saving = 2,
        CertificateOfDeposit = 3,
        CreditCard = 4,
        Others = 99
    }    
}