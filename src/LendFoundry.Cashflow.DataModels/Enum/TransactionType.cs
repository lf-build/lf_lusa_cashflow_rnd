﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Cashflow.DataModels.Enum
{
    /// <summary>
    /// Type of bank transaction
    /// </summary>
    public enum TransactionType
    {
        Credit = 1,
        Debit = 2,
        Others = 99
    }
}
