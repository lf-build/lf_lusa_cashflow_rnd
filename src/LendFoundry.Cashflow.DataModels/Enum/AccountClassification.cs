﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Cashflow.DataModels.Enum
{
    public enum AccountClassification
    {
        Personnal = 1,
        Corporate = 2,
        SmallBusiness = 3,
        Trust = 4,
        AddOnCard = 5,
        VistualCard = 6,
        Others = 99
    }
}
