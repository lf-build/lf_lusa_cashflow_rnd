﻿using LendFoundry.Cashflow.DataModels.Enum;
using System;

namespace LendFoundry.Cashflow.DataModels.Summary
{
    /// <summary>
    /// Monthly summary info
    /// </summary>
    public class MonthlySummaryModels
    {
        public CashflowMonth Month { get; set; }
        public int Year { get; set; }
        public Single TotalDepositAmount { get; set; }
        public Single TotalWithdrawalAmount { get; set; }
        public int TotalNumberofDeposits { get; set; }
        public int TotalNumberofWithdrawals { get; set; }
        public Single AverageDailyBalanace { get; set; }
        public int TotalNegativeDays { get; set; }
        public Single BeginningBalance { get; set; }
        public Single EndingBalance { get; set; }
        public Single TotalFees { get; set; }
        public Single TotalNonSufficientFundFees { get; set; } //TODO: Changed the name of the property
    }
}
