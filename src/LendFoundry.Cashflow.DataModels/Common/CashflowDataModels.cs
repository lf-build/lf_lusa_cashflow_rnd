﻿using System;
using LendFoundry.Cashflow.DataModels.Bank;

namespace LendFoundry.Cashflow.DataModels.Common
{
    public class CashflowDataModels
    {
        public BankAccountModels BankAccount { get; set; }
        public string Description { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
    }
}