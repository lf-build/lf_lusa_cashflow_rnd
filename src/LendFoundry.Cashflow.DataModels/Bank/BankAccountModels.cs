﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LendFoundry.Cashflow.DataModels.Enum;
using LendFoundry.Cashflow.DataModels.Customer;

namespace LendFoundry.Cashflow.DataModels.Bank
{
    /// <summary>
    /// Bank account inforamtion (No. of account) and customer inforamtion
    /// </summary>
    public class BankAccountModels
    {
        public BankModels Bank { get; set; }
        public List<AccountModels> Accounts { get; set; }
        public Single TotalBalance { get; set; }        
        public CashflowMonth FromMonth { get; set; }
        public int FromYear { get; set; }
        public CashflowMonth ToMonth { get; set; }
        public int ToYear { get; set; }
        public string CustomerReferenceNumber { get; set; }//Required field
    }
}
