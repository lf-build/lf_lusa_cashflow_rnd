﻿using LendFoundry.Cashflow.DataModels.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Cashflow.DataModels.Bank
{
    /// <summary>
    /// Amount with currency info
    /// </summary>
    public class AmountModels
    {
        public Single Amount { get; set; }//Required field
        public CurrencyCode Currency { get; set; }//Required field
    }
}
