﻿using LendFoundry.Cashflow.DataModels.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LendFoundry.Cashflow.DataModels.Bank
{
    /// <summary>
    /// Bank details
    /// </summary>
    public class BankModels
    {
        public string Name { get; set; } //Required field
        public string RoutingNumber { get; set; }
        public AddressModels Address { get; set; }
    }
}
