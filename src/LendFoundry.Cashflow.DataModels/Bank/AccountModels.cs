﻿using LendFoundry.Cashflow.DataModels.Customer;
using LendFoundry.Cashflow.DataModels.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Cashflow.DataModels.Bank
{
    /// <summary>
    /// Account details
    /// </summary   
    public class AccountModels
    {
        public string AccountNumber { get; set; } //Required field
        public BankAccountType AccountType { get; set; } //Required field
        public Single BeginningBalance { get; set; } //Required field
        public Single EndingBalance { get; set; } //Required field
        public bool IsActive { get; set; }
        public bool IsPrimary { get; set; }
        public CustomerModels Customer { get; set; }
        public List<StatementEntryModels> StatementEntries { get; set; } 
    }
}
