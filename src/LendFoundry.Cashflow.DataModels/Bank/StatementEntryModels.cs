﻿using LendFoundry.Cashflow.DataModels.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LendFoundry.Cashflow.DataModels.Bank
{
    /// <summary>
    /// Statement entry information
    /// </summary>
    public class StatementEntryModels
    {
        public string CheckNumber { get; set; }
        public AmountModels Amount { get; set; } //Required field
        public Single? EndingDailyBalance { get; set; }
        public DateTime Date { get; set; }//Required field
        public string MerchantName { get; set; }
        public string EntryNumber { get; set; }
        public CategoryType Category { get; set; }
        public TransactionType EntryBaseType { get; set; }//Required field
        public bool IsServiceFee { get; set; }
        public string Description { get; set; }//Required field
    }
}
