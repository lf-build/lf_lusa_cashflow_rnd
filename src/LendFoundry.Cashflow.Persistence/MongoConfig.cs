﻿using LendFoundry.Foundation.Persistence.Mongo;

namespace LendFoundry.Cashflow.Persistence
{
    public class MongoConfig : IMongoConfiguration
    {
        //private static string _connectionString = "mongodb://aarzoo:abc123@127.0.0.1:27017/";
        //private static IMongoClient _client;
        //private static IMongoDatabase _database;

        public string ConnectionString { get; set; }
        public string Database { get; set; }

        public IMongoConfiguration GetConfiguration()
        {
            return new MongoConfig()
            {
                ConnectionString = this.ConnectionString,
                Database = this.Database
            };
        }
    }
}
