﻿using LendFoundry.Foundation.Persistence.Mongo;
using System;
using System.Collections.Generic;
using System.Linq;
using LendFoundry.Cashflow.Abstractions;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.Serializers;
using MongoDB.Bson;
using LendFoundry.Cashflow.DataModels.Bank;
using LendFoundry.Cashflow.DataModels.Enum;
using LendFoundry.Cashflow.DataModels.Common;
using LendFoundry.Cashflow.DataModels.Customer;
using System.Linq.Expressions;
using LendFoundry.Tenant.Client;
using MongoDB.Driver;
using MongoDB.Driver.Linq;

namespace LendFoundry.Cashflow.Persistence
{
    public class CashflowDataRepository : MongoRepository<ICashflowData, CashflowData>, ICashflowDataRepository
    {
        static CashflowDataRepository()
        {
            BsonClassMap.RegisterClassMap<CashflowDataModels>(map =>
            {
                map.AutoMap();

                var type = typeof(CashflowDataModels);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
                map.SetIsRootClass(true);
            });

            BsonClassMap.RegisterClassMap<CashflowData>(map =>
            {
                map.AutoMap();

                var type = typeof(CashflowData);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
                map.SetIsRootClass(true);
            });

            BsonClassMap.RegisterClassMap<BankAccountModels>(map =>
            {
                map.AutoMap();
                map.MapMember(p => p.FromMonth).SetSerializer(new EnumSerializer<CashflowMonth>(BsonType.String));
                map.MapMember(p => p.ToMonth).SetSerializer(new EnumSerializer<CashflowMonth>(BsonType.String));

                var type = typeof(BankAccountModels);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
            });

            BsonClassMap.RegisterClassMap<BankModels>(map =>
            {
                map.AutoMap();

                var type = typeof(BankModels);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
            });

            BsonClassMap.RegisterClassMap<AccountModels>(map =>
            {
                map.AutoMap();
                map.MapMember(p => p.AccountType).SetSerializer(new EnumSerializer<BankAccountType>(BsonType.String));

                var type = typeof(AccountModels);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
            });

            BsonClassMap.RegisterClassMap<CustomerModels>(map =>
            {
                map.AutoMap();

                var type = typeof(CustomerModels);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
            });

            BsonClassMap.RegisterClassMap<CustomerNameModels>(map =>
            {
                map.AutoMap();
                map.MapMember(p => p.Position).SetSerializer(new EnumSerializer<AccountHolderPosition>(BsonType.String));

                var type = typeof(CustomerNameModels);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
            });

            BsonClassMap.RegisterClassMap<AddressModels>(map =>
            {
                map.AutoMap();

                var type = typeof(AddressModels);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
            });

            BsonClassMap.RegisterClassMap<StatementEntryModels>(map =>
            {
                map.AutoMap();
                map.MapMember(p => p.Category).SetSerializer(new EnumSerializer<CategoryType>(BsonType.String));
                map.MapMember(p => p.EntryBaseType).SetSerializer(new EnumSerializer<TransactionType>(BsonType.String));

                var type = typeof(StatementEntryModels);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
            });

            BsonClassMap.RegisterClassMap<AmountModels>(map =>
            {
                map.AutoMap();
                map.MapMember(p => p.Currency).SetSerializer(new EnumSerializer<CurrencyCode>(BsonType.String));

                var type = typeof(AmountModels);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
            });
        }

        public CashflowDataRepository(ITenantService tenantService, IMongoConfiguration configuration) :
            base(tenantService, configuration, "cashflowData")
        { }

        public IEnumerable<T> All<T>(Expression<Func<T, bool>> query) where T : ICashflowData
        {
            return Collection.AsQueryable().OfType<T>().Where(query);
        }

        public List<T> GetCashflowData<T>(Expression<Func<T, bool>> query) where T : ICashflowData
        {
            return  Collection.AsQueryable().OfType<T>().Where(query).ToList();
        }

        public IMongoCollection<ICashflowData> GetCollection()
        {
            return Collection;
        }

        public async void Add(IEnumerable<ICashflowData> cashflowData)
        {
            if (cashflowData == null)
                throw new ArgumentNullException(nameof(cashflowData));

            var entries = new List<ICashflowData>(cashflowData);
            entries.ForEach(p =>
            {
                p.Id = ObjectId.GenerateNewId().ToString();
                p.TenantId = TenantService.Current.Id;
            });

            await Collection.InsertManyAsync(entries);
        }

        public async void Update(IEnumerable<ICashflowData> cashflowData)
        {
            var bulk = new List<WriteModel<ICashflowData>>();
            foreach (var data in cashflowData)
            {
                var filter = new ExpressionFilterDefinition<ICashflowData>(p =>
                    p.TenantId == TenantService.Current.Id &&
                    p.TenantId == data.TenantId &&
                    p.Id == data.Id);
                bulk.Add(new ReplaceOneModel<ICashflowData>(filter, data));
            }
            await Collection.BulkWriteAsync(bulk);
        }
    }
}
